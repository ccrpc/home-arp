---
title: Home
draft: false
---

# Urbana HOME Consortium HOME-ARP Funds

#### Welcome!

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.


### Functional Classification

Functional classification is a process of categorizing roads primarily based on
mobility and accessibility. Mobility relates to operating speed, level of
service, and riding comfort. It is a measure of the expedited traffic conditions
between origin and destination. Accessibility identifies travel conditions with
increased exit and entry locations. IDOT uses the following classifications for
roadways throughout the state: 

{{<image src="Mobilitiy and access.jpg"
  link="/destination"
  alt="Figure describes relationship between mobility and access"
  attr="Mobility vs. Access" attrlink="https://ops.fhwa.dot.gov/access_mgmt/presentations/am_principles_intro/index.htm"
  position="right">}}